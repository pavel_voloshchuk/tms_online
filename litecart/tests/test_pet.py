import allure
from requests import post, get, delete

link = "https://petstore.swagger.io/v2/"


def add_new_pet(pet: dict):
    r = post(f"{link}pet", json=pet)
    return r.status_code


def get_pet(pet_id: int):
    r = get(f"{link}pet/{pet_id}")
    return r.status_code


def delete_pet(id_pet: int):
    r = delete(f"{link}pet/{id_pet}")
    return r.status_code


@allure.feature("api")
@allure.story("pet")
def test_pet():
    new_pet = {
        "id": 666999,
        "category": {
            "id": 0,
            "name": "string"
        },
        "name": "pepa",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 0,
                "name": "string"
            }
        ],
        "status": "available"
    }

    with allure.step("add pet"):
        assert add_new_pet(new_pet) == 200, 'add pet failed'
    with allure.step("check added pet"):
        assert get_pet(666999) == 200, 'check pet failed'
    with allure.step("check delete pet"):
        assert delete_pet(666999) == 200, 'delete pet failed'
    with allure.step("check pet was deleted"):
        assert get_pet(666999) == 404, "pet doesn't deleted"
