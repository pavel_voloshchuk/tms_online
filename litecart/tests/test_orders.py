import allure
from mysql import connector


def connect_db(db):
    cursor = db.cursor()
    print("db connect")
    return cursor


def close_db(db):
    print("db close")
    db.close()


def get_all_orders(table_name: str, cursor):
    cursor.execute(f"SELECT customer_firstname, customer_lastname, "
                   f"weight_total, date_created "
                   f"FROM {table_name} WHERE customer_firstname = 'Ivan'")
    for row in cursor.fetchall():
        return row


def delete_order(table_name: str, cursor):
    dell = f"DELETE FROM {table_name} WHERE customer_firstname = 'Ivan'"

    cursor.execute(dell)


@allure.feature("order test")
@allure.story("ducks")
def test_orders():
    db_arg = connector.connect(
        host="localhost",
        user="root",
        password="",
        database="litecart")

    table_arg = "lc_orders"
    cursor_arg = connect_db(db_arg)
    with allure.step("correct login"):
        assert 'Ivan' in str(get_all_orders(table_arg, cursor_arg)), 'alarm'
    delete_order(table_arg, cursor_arg)
    close_db(db_arg)
