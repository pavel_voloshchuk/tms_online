import time

import allure

from litecart.page_objects.main_page import MainPage
from selenium.webdriver.support.ui import Select

from litecart.page_locators.yellow_duck_page_locators import \
    YellowDuckPageLocators


@allure.feature("order test")
@allure.story("ducks")
def test_by_duck(driver):
    page = MainPage(driver)
    page.open()
    page.send_email()
    page.send_pass()
    page.login_button().click()

    with allure.step("correct login"):
        assert page.get_notice_success().text == "You are now logged in as " \
                                                 "Ivan " \
                                                 "Ivanov.", "Incorrect login"
    yellow_duck_page = page.open_yellow_duck()
    with allure.step("correct Yellow Duck page"):
        assert page.get_title() == "Yellow Duck | Subcategory | " \
                                   "Rubber Ducks | My Store", \
            "Incorrect fiction page title! "
    select_size = Select(
        yellow_duck_page.find_element(YellowDuckPageLocators.SIZE_LOCATORS)
    )
    select_size.select_by_value('Small')
    select_quantity = page.find_element(
        YellowDuckPageLocators.QUANTITY_LOCATORS
    )
    select_quantity.clear()
    select_quantity.send_keys("3")
    add_product_button = yellow_duck_page.find_element(
        YellowDuckPageLocators.ADD_PRODUCT_BUTTON_LOCATORS
    )
    add_product_button.click()
    time.sleep(1)
    checkout_page = page.open_checkout()
    with allure.step("correct Checkout title"):
        assert checkout_page.get_title() == "Checkout | My Store", \
            "Incorrect fiction page title!"
    with allure.step("correct count"):
        assert checkout_page.get_quantity_ducks().text == "3", "Incorrect " \
                                                               "quantity "
    with allure.step("correct payment due"):
        assert checkout_page.get_payment_due().text == "39.21 €", \
            "Incorrect payment due"
    checkout_page.confirm_button_click()
