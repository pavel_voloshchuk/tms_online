import allure
from selenium.webdriver.support.ui import Select
from litecart.page_objects.main_page import MainPage
from litecart.page_locators.regional_settings_locators import \
    RegionalSettingsLocators


@allure.feature("main settings")
@allure.story("Change country and currency")
def test_regional_settings_params(driver):
    page = MainPage(driver)
    page.open()
    page.open_regional_settings()
    regional_settings_page = page.open_regional_settings()
    country_form = Select(regional_settings_page.find_element(
        RegionalSettingsLocators.COUNTRY_FORM_LOCATORS
    ))
    country_form.select_by_visible_text("Belarus")
    currency_form = Select(regional_settings_page.find_element(
        RegionalSettingsLocators.CURRENCY_FORM_LOCATORS
    )
    )
    currency_form.select_by_visible_text("Euros")
    save_button = regional_settings_page.find_element(
        RegionalSettingsLocators.SETTINGS_SAVE)
    save_button.click()
    with allure.step("correct title"):
        assert page.get_title() == "Online Store | My Store", \
            "Incorrect fiction page title!"
    with allure.step("correct country"):
        assert page.get_country().text == "Belarus", \
            "Incorrect country"
    with allure.step("correct country"):
        assert page.get_currency().text == "EUR", \
            "Incorrect currency"
