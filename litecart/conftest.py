import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope="session")
def driver():
    return Chrome()
