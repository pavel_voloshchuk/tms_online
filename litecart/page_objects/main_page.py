from litecart.page_locators.main_page_locators import \
    MainPageLocators
from litecart.page_objects.checkout_page import CheckoutPage
from litecart.page_objects.yellow_duck_page import YellowDuckPage
from litecart.page_objects.base_page import BasePage
from litecart.page_objects.regional_settings_page import \
    RegionalSettingsPage


class MainPage(BasePage):
    URL = "http://localhost:8080/litecart/en/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_regional_settings(self):
        regional_settings_link = self.find_element(
            MainPageLocators.REGIONAL_SETTINGS_PAGE_LOCATORS)
        regional_settings_link.click()
        return RegionalSettingsPage(self.driver, self.driver.current_url)

    def get_title(self):
        return self.driver.title

    def get_currency(self):
        return self.find_element(MainPageLocators.CURRENCY_LOCATORS)

    def get_country(self):
        return self.find_element(MainPageLocators.COUNTRY_LOCATORS)

    def send_email(self):
        return self.find_element(
            MainPageLocators.LOGIN_EMAIL_LOCATORS).send_keys("test@test.com")

    def send_pass(self):
        return self.find_element(
            MainPageLocators.LOGIN_PASSWORD_LOCATORS).send_keys("test")

    def login_button(self):
        return self.find_element(MainPageLocators.LOGIN_BUTTON_LOCATORS)

    def get_notice_success(self):
        return self.find_element(MainPageLocators.LOGIN_SUCCESS)

    def open_yellow_duck(self):
        yellow_duck_link = self.find_element(MainPageLocators.YELLOW_DUCK_LINK)
        yellow_duck_link.click()
        return YellowDuckPage(self.driver, self.driver.current_url)

    def open_checkout(self):
        checkout_link = self.find_element(MainPageLocators.CHECKOUT_LOCATORS)
        checkout_link.click()
        return CheckoutPage(self.driver, self.driver.current_url)
