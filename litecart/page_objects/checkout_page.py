from litecart.page_objects.base_page import BasePage
from litecart.page_locators.checkout_page_locators import \
    CheckoutPageLocators


class CheckoutPage(BasePage):
    def get_title(self):
        return self.driver.title

    def get_quantity_ducks(self):
        return self.find_element(CheckoutPageLocators.QUANTITY_DUCKS)

    def get_payment_due(self):
        return self.find_element(CheckoutPageLocators.PAYMENT_DUE)

    def confirm_button_click(self):
        confirm_button = self.find_element(
            CheckoutPageLocators.CONFIRM_ORDER_BUTTON
        )
        return confirm_button.click()
