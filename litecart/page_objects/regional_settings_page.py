from litecart.page_objects.base_page import BasePage
from litecart.page_locators.regional_settings_locators import \
    RegionalSettingsLocators


class RegionalSettingsPage(BasePage):

    def get_title(self):
        return self.driver.title

    def get_country_form(self):
        return self.find_element(
            RegionalSettingsLocators.COUNTRY_FORM_LOCATORS
        )

    def get_currency_form(self):
        return self.find_element(
            RegionalSettingsLocators.CURRENCY_FORM_LOCATORS
        )
