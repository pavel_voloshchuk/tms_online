from litecart.page_objects.base_page import BasePage


class YellowDuckPage(BasePage):
    def get_title(self):
        return self.driver.title
