from selenium.webdriver.common.by import By


class YellowDuckPageLocators:
    SIZE_LOCATORS = (By.NAME, "options[Size]")
    QUANTITY_LOCATORS = (By.NAME, "quantity")
    ADD_PRODUCT_BUTTON_LOCATORS = (By.NAME, "add_cart_product")
