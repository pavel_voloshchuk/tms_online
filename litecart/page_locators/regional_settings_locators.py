from selenium.webdriver.common.by import By


class RegionalSettingsLocators:
    COUNTRY_FORM_LOCATORS = (By.NAME, 'country_code')
    CURRENCY_FORM_LOCATORS = (By.NAME, 'currency_code')
    SETTINGS_SAVE = (By.NAME, 'save')
