from selenium.webdriver.common.by import By


class MainPageLocators:
    REGIONAL_SETTINGS_PAGE_LOCATORS = (
        By.XPATH, "/html/body/div[3]/footer/table/tbody"
                  "/tr/td[3]/nav/ul/li[2]/a")
    CURRENCY_LOCATORS = (
        By.XPATH, "/html/body/div[1]/div/header/div[2]/div/div[2]"
    )
    COUNTRY_LOCATORS = (
        By.XPATH, "/html/body/div[1]/div/header/div[2]/div/div[3]"
    )
    LOGIN_EMAIL_LOCATORS = (By.XPATH, "//input[@name='email']")
    LOGIN_PASSWORD_LOCATORS = (By.XPATH, "//input[@name='password']")
    LOGIN_BUTTON_LOCATORS = (By.NAME, "login")
    LOGIN_SUCCESS = (
        By.XPATH, "/html/body/div[2]/div/div[2]/div/div[1]/div/div/div")
    YELLOW_DUCK_LINK = (By.XPATH, "//a[@title='Yellow Duck']")
    CHECKOUT_LOCATORS = (By.ID, "cart")
