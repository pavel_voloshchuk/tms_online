from selenium.webdriver.common.by import By


class CheckoutPageLocators:
    QUANTITY_DUCKS = (By.XPATH, "//*[@id='order_confirmation-wrapper']/table"
                                "/tbody/tr[2]/td[1]")
    PAYMENT_DUE = (By.XPATH, "//*[@id='order_confirmation-wrapper']/table"
                             "/tbody/tr[2]/td[6]")
    CONFIRM_ORDER_BUTTON = (By.NAME, "confirm_order")
